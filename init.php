<?php
/**
 * Initialization of project
 *
 * @author Vitaliy Pyatin <mail.pyvil@gmail.com>
 */
// autoloader
$loader = require_once 'vendor/autoload.php';
use core\App;
$app = new App();

$runner = $app->getOrm();

/**
 * create patient table
 */
$runner->execute('
CREATE TABLE IF NOT EXISTS patient (
  `patient_id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `parent_id` INT UNSIGNED NOT NULL,
  `firstname` VARCHAR(100) NOT NULL,
  `middlename` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `address` TEXT,
  `telephone` VARCHAR(50),
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth_key` VARCHAR(32) NOT NULL,
  `password_reset_token` VARCHAR(255) NOT NULL
) CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB;
');

/**
 * doctor table
 */
$runner->execute('
CREATE TABLE IF NOT EXISTS doctor (
  `docktor_id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `firstname` VARCHAR(100) NOT NULL,
  `middlename` VARCHAR(100) NOT NULL,
  `lastname` VARCHAR(100) NOT NULL,
  `username` VARCHAR(100) NOT NULL,
  `email` VARCHAR(100) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `position` VARCHAR(200) NOT NULL,
  `telephone` VARCHAR(50),
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `auth_key` VARCHAR(32) NOT NULL,
  `password_reset_token` VARCHAR(255) NOT NULL
) CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB
');

/**
 * ticket table
 */
$runner->execute('
CREATE TABLE IF NOT EXISTS ticket (
  `ticket_id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `patient_id` INT UNSIGNED,
  `doctor_id` INT UNSIGNED,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
) CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB;
CONSTRAINT `FK_ticket_to patient` FOREIGN KEY (patient_id) REFERENCES TO patient(patient_id) ON UPDATE CASCADE;
CONSTRAINT `FK_ticket_to doctor` FOREIGN KEY (doctor_id) REFERENCES TO doctor(doctor_id) ON UPDATE CASCADE;
');

/**
 *
 */
$runner->execute('
CREATE TABLE IF NOT EXISTS answer (
  `answer_id` INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  `ticket_id` INT UNSIGNED,
  `patient_id` INT UNSIGNED,
  `doctor_id` INT UNSIGNED,
  `answer_text` TEXT NOT NULL,
  `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
) CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB;
CONSTRAINT `FK_answer_to patient` FOREIGN KEY answer(patient_id) REFERENCES TO patient(patient_id) ON UPDATE CASCADE;
CONSTRAINT `FK_answer_to doctor` FOREIGN KEY answer(doctor_id) REFERENCES TO doctor(doctor_id) ON UPDATE CASCADE;
CONSTRAINT `FK_answer_to ticket` FOREIGN KEY answer(ticket_id) REFERENCES TO ticket(ticket_id) ON UPDATE CASCADE;
');

unset($runner);
unset($app);