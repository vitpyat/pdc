<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.06.16
 * Time: 20:50
 */

namespace local\controller;


use core\App;
use core\http\Controller;
use core\Session;
use local\helper\UserHelper;
use local\model\Patient;

class HomeController extends Controller
{
    /**
     * @return string
     */
    public function indexAction()
    {
        return $this->render('home/index');
    }

    /**
     * @param $action
     *
     * @return string
     */
    public function loginAction($action)
    {
        return $this->render('home/login/' . $action[0]);
    }

    /**
     * @param $action
     *
     * @return bool
     * @throws \Exception
     */
    public function registerAction($action)
    {
        $action = $action[0];
        $reg = App::getRequest()->getPosts();
        $helper = new UserHelper();
        $model = null;
        if ($action == 'patient') {
            $model = new Patient();
        } elseif ($action == 'doctor') {
            $model = new Doctor();
        } else {
            throw new \Exception('Something went wrong, 404');
        }
        $model->loadFromRequest($reg);
        if (!$helper->comparePasswords($model->getPassword(), $reg['confirm-password'])) {
            App::getRequest()->redirect('/index.php/home/login/' . $action);
            return false;
        }
        $model->addAdditional();
        if ($model->save()) {
            App::getRequest()->redirect('/index.php/profile/id/' . $model->getData($model->primaryKey));
            return true;
        }
        throw new \Exception('Something went wrong wile saving');
    }

    public function logingAction($action)
    {
        $action = $action[0];
        $log = App::getRequest()->getPosts();
        if ($action == 'patient') {
            $model = new Patient();
        } elseif ($action == 'doctor') {
            $model = new Doctor();
        } else {
            throw new \Exception('Something went wrong, 404');
        }
        $log = $log[ucfirst($model->getTableName())];
        $user = Session::login($model, $log);
        if ($user) {
            App::getRequest()->redirect('/profile/id/' . $user->getData($user->primaryKey));
        }
    }
}