<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.06.16
 * Time: 20:50
 */

namespace local\controller;


use core\App;
use core\http\Controller;
use core\Session;
use local\helper\UserHelper;
use local\model\Patient;

class ProfileController extends Controller
{
    protected $_layout = 'app/local/view/layout/profile';

    public function idAction($id)
    {
        if (!$this->isAllow()) {
            App::getRequest()->redirect('/');
            return false;
        }
        $user = Session::getUserId() == $id ? Session::getUser() : new Patient($id);
        return $this->render('profile/id', array('user' => $user));
    }
}