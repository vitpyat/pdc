<?php
/**
 * Created by PhpStorm.
 * User: pyvil
 * Date: 05.07.16
 * Time: 23:14
 */

namespace local\model;

use core\App;
use core\db\Model;

class Patient extends Model
{
    /**
     * @var string
     */
    public $primaryKey = 'patient_id';
    /**
     * Get current table name
     *
     * @return string
     */
    public function getTableName()
    {
        return 'patient';
    }

    /**
     * @throws \Exception
     *
     * @return void
     */
    public function addAdditional()
    {
        $this->setData('created_at', date("Y-m-d H:i:s"));
        $this->setData('updated_at', date("Y-m-d H:i:s"));
        $this->setData('auth_key', App::generateRandomString());
        $this->setData('password_reset_token', App::generateRandomString() . '_' . time());
        $this->setData('password', App::passwordHash($this->getData('password')));
    }
}