<?php

namespace local\helper;


class UserHelper
{
    /**
     * @param $password
     * @param $confirm
     *
     * @return bool
     */
    public function comparePasswords($password, $confirm)
    {
        return $password === $confirm;
    }
}