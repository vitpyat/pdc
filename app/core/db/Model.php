<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.06.16
 * Time: 18:12
 */

namespace core\db;


use ArrayAccess;
use ArrayIterator;
use IteratorAggregate;
use Traversable;

abstract class Model extends orm\Orm implements IteratorAggregate, ArrayAccess
{
    public $primaryKey = 'id';
    /**
     * @var array
     */
    protected $_where = null;
    /**
     * @var array
     */
    protected $_select = null;
    /**
     * @var array
     */
    protected $_join = null;
    /**
     * @var string
     */
    protected $_orderBy = null;
    /**
     * @var string
     */
    protected $_groupBy = null;

    public function __construct($id = null)
    {
        parent::__construct();

        if (!is_null($id)) {
            return $this->where(array($this->primaryKey => $id))->load();
        }
        return $this;
    }


    /**
     * Get current table name
     *
     * @return string
     */
    public abstract function getTableName();

    /**
     * @param array $data data
     * @return $this
     */
    public function where(array $data)
    {
        $this->_where = $data;
        return $this;
    }

    /**
     * @param array $data data
     * @return $this
     */
    public function andWhere(array $data)
    {
        $this->_where += $data;
        return $this;
    }

    /**
     * @param array $select
     * @return $this
     * @throws \Exception
     */
    public function select($select = array('*'))
    {
        if (is_array($select)) {
            $this->_select = implode(',', $select);
        } elseif (is_string($select)) {
            $this->_select = $select;
        } else {
            throw new \Exception('Not valid select params');
        }
        return $this;
    }
    /**
     * @param array $join
     * @return $this
     */
    public function join(array $join)
    {
        $type = 'INNER';
        $table = $join[0];
        $on = $join[1];
        if (isset($join[2])) {
            $type = $join[2];
        }
        if (is_null($this->_join)) {
            $this->_join = array();
        }
        $this->_join += array('type' => $type, 'table' => $table, 'on' => $on);
        return $this;
    }

    /**
     * @param string $field
     * @return $this
     */
    public function groupBy($field = 'id')
    {
        $this->_groupBy = ' GROUP BY `' . $field . '`';
        return $this;
    }

    /**
     * @param string $field
     * @param string $dir
     * @return $this
     */
    public function orderBy($field = 'id', $dir = 'asc')
    {
        $this->_orderBy = ' ORDER BY `' . $field . '` ' . strtoupper($dir);
        return $this;
    }
    /**
     * @return $this
     * @throws \Exception
     */
    public function load()
    {
        if (is_null($this->_select)) {
            $this->_select = array('*');
        }
        $select = implode(', ', $this->_select);
        $select = 'SELECT ' . $select . ' FROM `' . $this->getTableName() . '`';
        $join = '';
        if (!is_null($this->_join)) {
            foreach ($this->_join as $_join) {
                $join .= strtoupper($_join['type']). ' JOIN `' . $_join['table'] . '` ON ' . $_join['on'] . ' ';
            }
        }
        $select .= ' ' . $join;
        $select .= ' ' . $this->_orderBy;
        $select .= ' ' . $this->_groupBy;

        return $this->_prepareLoadData($this->execute($select));
    }

    /**
     * @param array $data
     * @return $this
     */
    public function loadFromRequest(array $data)
    {
        $tableIndex = ucfirst($this->getTableName());
        $data = isset($data[$tableIndex]) ? $data[$tableIndex] : $data;
        foreach ($data as $field => $value) {
            $this->setData($field, htmlspecialchars($value));
        }
        return $this;
    }

    /**
     * Prepare data
     * @param array $data
     * @return $this
     */
    protected function _prepareLoadData(array $data)
    {
        foreach ($data[0] as $field => $value) {
            $this->_origData[$field] = htmlspecialchars($value);
            $this->_data[$field] = htmlspecialchars($value);
        }
        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        if (is_null($this->_origData)) {
            $query = 'INSERT INTO `' . $this->getTableName() . '` (`' . implode('`,`', array_keys($this->_data)) . '`)' .
                ' VALUES (\'' . implode('\',\'', array_values($this->_data)) . '\')';
        } else {
            $query = 'UPDATE `' . $this->getTableName() . '` ';
            $new = array();
            foreach ($this->_data as $field => $value) {
                $new[] = '`' . $field . '`="' . $value . '"';
            }
            $query .= implode(', ', $new) . ' WHERE `id`='.$this->_origData[$this->primaryKey];
        }
        $this->execute($query);
        return $this->where(array($this->primaryKey => $this->_db->lastInsertId()))->load();
    }
    
    /**
     * Returns an iterator for traversing the attributes in the model.
     * This method is required by the interface [[\IteratorAggregate]].
     * @return ArrayIterator an iterator for traversing the items in the list.
     */
    public function getIterator()
    {
        $data = $this->getData();
        return new ArrayIterator($data);
    }

    /**
     * Returns whether there is an element at the specified offset.
     * This method is required by the SPL interface [[\ArrayAccess]].
     * It is implicitly called when you use something like `isset($model[$offset])`.
     * @param mixed $offset the offset to check on
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return $this->getData($offset) !== null;
    }

    /**
     * Returns the element at the specified offset.
     * This method is required by the SPL interface [[\ArrayAccess]].
     * It is implicitly called when you use something like `$value = $model[$offset];`.
     * @param mixed $offset the offset to retrieve element.
     * @return mixed the element at the offset, null if no element is found at the offset
     */
    public function offsetGet($offset)
    {
        return $this->getData($offset);
    }

    /**
     * Sets the element at the specified offset.
     * This method is required by the SPL interface [[\ArrayAccess]].
     * It is implicitly called when you use something like `$model[$offset] = $item;`.
     * @param integer $offset the offset to set element
     * @param mixed $value the element value
     */
    public function offsetSet($offset, $value)
    {
       $this->setData($offset, $value);
    }

    /**
     * Sets the element value at the specified offset to null.
     * This method is required by the SPL interface [[\ArrayAccess]].
     * It is implicitly called when you use something like `unset($model[$offset])`.
     * @param mixed $offset the offset to unset element
     */
    public function offsetUnset($offset)
    {
        $this->setData($offset, null);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return serialize($this->getData());
    }


}