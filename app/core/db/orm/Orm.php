<?php

namespace core\db\orm;

use core\App;
use Exception;
use PDO;
use PDOException;

/**
 * Class Orm
 *
 * @package core\db\orm
 */
class Orm
{
    /**
     * @var array
     */
    protected $_config = array();
    /**
     * @var null
     */
    protected $_db = null;
    /**
     * @var array
     */
    protected $_data = null;
    /**
     * @var array
     */
    protected $_origData = null;

    /**
     * Orm constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        try {
            $this->_config = App::getConfig('db');
            $this->_db = new PDO(
                "mysql:host={$this->_config['host']};dbname={$this->_config['db_name']};charset=utf8",
                $this->_config['username'],
                $this->_config['password']
            );
            $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->_data = array();
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param null $key
     *
     * @return array
     */
    public function getData($key = null)
    {
        if ($key === null) {
            return $this->_data;
        }
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }

        return null;
    }

    /**
     * @param mixed $key
     * @param mixed $data
     *
     * @return $this
     */
    public function setData($key, $data)
    {
        $this->_data[$key] = $data;

        return $this;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasData($name)
    {
        return $this->getData($name) !== null;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return array
     */
    public function __call($name, $arguments)
    {
        $val = strtolower(substr($name, 0, 3));
        $name = strtolower(substr($name, 3, strlen($name) - 3));
        switch ($val) {
            case 'get' :
                return $this->getData($name);
            case 'set' :
                $this->setData($name, $arguments);
                return $this;
            case 'has' :
                return $this->hasData($name);
        }
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function __get($name)
    {
        return $this->getData($name);
    }

    /**
     * Execute statement
     *
     * @param string $string value to execute
     * @param array $params params
     *
     * @return array
     * @throws Exception
     */
    public function execute($string, array $params = array())
    {
        $string = trim($string);
        $this->_db->beginTransaction();
        try {
            $stmt = $this->_db->prepare($string);
            $stmt->execute($params);
            $result = array(0 => array());
            if ($stmt->rowCount()) {
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            $this->_db->commit();
            return $result;
        } catch (PDOException $e) {
            $this->_db->rollBack();
            throw new PDOException($e->getMessage(), $e->getCode());
        } catch (Exception $e) {
            $this->_db->rollBack();
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}