<?php

namespace core\http;
use core\App;

/**
 * Class Request
 *
 * Handling request from browser
 *
 * @package core\http
 */
class Request
{
    /**
     * GET params
     *
     * @var array
     */
    protected $_params = array();

    /**
     * POST params
     *
     * @var array
     */
    protected $_post = array();

    /**
     * Request constructor
     *
     */
    public function __construct()
    {
        $this->_post = $_POST;
        $this->_params = $_GET;
    }

    /**
     * Get HTTP method
     *
     * @return string
     */
    public function getMethod()
    {
        if (isset($_POST['_method'])) {
            return strtoupper($_POST['_method']);
        }

        if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
            return strtoupper($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']);
        }

        if (isset($_SERVER['REQUEST_METHOD'])) {
            return strtoupper($_SERVER['REQUEST_METHOD']);
        }

        return 'GET';
    }

    /**
     * Is ajax request
     * 
     * @return bool
     */
    public function getIsAjax()
    {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }

    /**
     * Get GET param by key
     *
     * @param $key
     *
     * @return mixed|null
     */
    public function getParam($key)
    {
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return null;
    }

    /**
     * Get all GET params
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Get POST data by key
     *
     * @param $key
     *
     * @return mixed|null
     */
    public function getPost($key)
    {
        if (isset($this->_post[$key])) {
            return $this->_post[$key];
        }
        return null;
    }

    /**
     * Get all POST data
     *
     * @return array
     */
    public function getPosts()
    {
        return $this->_post;
    }

    /**
     * Get request string
     *
     * @return string
     */
    protected function _getUriPaths()
    {
        if(!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }

        if(!empty($_SERVER['PATH_INFO'])) {
            return trim($_SERVER['PATH_INFO'], '/');
        }

        if(!empty($_SERVER['QUERY_STRING'])) {
            return trim($_SERVER['QUERY_STRING'], '/');
        }
    }

    /**
     *
     * @param string $path
     * @param int    $code
     *
     * @return void
     */
    public function redirect($path, $code = 302)
    {
        if (!$path || ($path == '')) {
            $path = '/index.php';
        }
        header("Location: {$path}", true);
        exit();
    }

    /**
     * Handle request
     *
     * @return array|bool
     */
    public function handle()
    {
        $url = parse_url($this->_getUriPaths());
        $url['path'] = str_replace('index.php/', '', $url['path']);
//        if (file_exists('app/local/' . $url['path']) && $url['path'] != '') {
//            echo file_get_contents('app/local/' . $url['path']);
//            return;
//        }
        $parts = array();
        $config = App::getConfig('router')['default'];
        if (!empty( $url)) {
            $schema = isset($url['scheme']) ? $url['scheme'] : 'http';
            $parts = explode('/', $url['path']);
        }
        $controller = ucfirst(array_shift($parts)) . 'Controller';
        $action = array_shift($parts) . 'Action';
        $params = $parts;

        if ($controller === 'Controller') {
            $controller = ucfirst($config['controller']) . 'Controller';
        }

        if ($action === 'Action') {
            $action = $config['action'] . 'Action';
        }
        return array('controller' => $controller, 'action' => $action, 'params' => $params);
    }
}