<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.06.16
 * Time: 18:52
 */

namespace core\http;


use core\App;
use core\Session;
use core\View;

abstract class Controller
{
    /**
     * Layout path
     *
     * @var string
     */
    protected $_layout = 'app/local/view/layout/default';

    /**
     * View instance
     *
     * @var View
     */
    protected $_view;

    /**
     * Get view instance
     *
     * @return View
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->_view = new View();
    }

    /**
     * Render layout
     *
     * @param string $content
     *
     * @return mixed
     */
    public function renderContent($content)
    {
        $layout = $this->_layout;
        if ($layout !== null) {
            $content = $this->getView()->renderFile($layout, array('content' => $content));
        }
        return $content;
    }

    /**
     * Render view
     *
     * @param string $file
     * @param array  $params
     *
     * @return string
     */
    public function render($file, array $params = array())
    {
        $content = $this->getView()->renderFile($this->getView()->getPath() . $file, $params);
        return $this->renderContent($content);
    }

    /**
     * @return bool
     */
    public function isAllow()
    {
        return Session::getIsLoggedIn();
    }
}