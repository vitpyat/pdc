<?php

namespace core;

/**
 * Class View
 * 
 * @package core
 */
class View
{
    /**
     * @var string
     */
    protected $_path = 'app/local/view/template/';

    /**
     * View constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get view path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * @param string $viewFile
     * @param array  $params
     *
     * @return mixed
     */
    public function renderFile($viewFile, array $params)
    {
        ob_start();
        ob_implicit_flush(false);
        extract($params, EXTR_OVERWRITE);
        include ($viewFile . '.phtml');
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}